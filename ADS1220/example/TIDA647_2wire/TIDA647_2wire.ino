/* --COPYRIGHT--,BSD
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/*******************************************************************************
 *
 * TIDA_00647_UART.ino
 *
 * Demo to send the TIDA_00647 Data via a UART 
 *
 * Aug 2015
 * MCV (Matt-CHV)
 *
 *
 ******************************************************************************/

#include <SPI.h>
#include <ADS1220.h>
#define LED RED_LED
#define RDY P3_0
#define CS P1_3

ADS1220 myADS1220_647(CS);

volatile int count = 0;
volatile int temp = 0;

volatile static unsigned char tempData[3];

void setup() {
  //Delay to allow power supplies to settle and power-up reset to complete; minimum of 50 μs;
  delay(1);  
  //Configure the SPI interface of the microcontroller to SPI mode 1 (CPOL = 0, CPHA = 1);
    SPI.begin();                     // SCK, MOSI set to LOW
    SPI.setDataMode(SPI_MODE1);      // SPI mode 1 config
    SPI.setBitOrder(MSBFIRST);
    SPI.setClockDivider(128);        // SPI clock frequency
    //If the CS pin is not tied low permanently, configure the microcontroller GPIO connected to CS as an output;
    pinMode(CS, OUTPUT);
    digitalWrite(CS, HIGH);
    //Configure the microcontroller GPIO connected to the DRDY pin as a falling edge triggered interrupt input;
    pinMode(RDY, INPUT_PULLUP);
    attachInterrupt(RDY, counting, FALLING); // Interrupt is fired whenever button is pressed

    //Set CS to the device low;
    digitalWrite(CS, LOW);
    //Delay for a minimum of td(CSSC);
    delay(1);
    //send the RESET command
    myADS1220_647.ADS1220_Reset();
    //delay
    delay(1);
    //Write the respective register configuration with the WREG command (43h, 08h, 04h, 10h, and 00h);
  //    myADS1220_647.init (ADS1220_MUX_AIN0_AIN1, ADS1220_OP_MODE_NORMAL,             ADS1220_CONVERSION_CONTINUOUS, ADS1220_DATA_RATE_20SPS, ADS1220_GAIN_16, ADS1220_USE_PGA,             ADS1220_IDAC1_AIN3, ADS1220_IDAC2_AIN2, ADS1220_IDAC_CURRENT_250_UA);
    myADS1220_647.ADS1220_initfull(ADS1220_MUX_AIN0_AIN1, ADS1220_GAIN_16, ADS1220_USE_PGA,
					ADS1220_DATA_RATE_20SPS, ADS1220_OP_MODE_NORMAL, ADS1220_CONVERSION_CONTINUOUS,  ADS1220_TEMP_SENSOR_OFF, ADS1220_BURN_OUT_CURRENT_OFF,
					ADS1220_FIR_50_60, ADS1220_VREF_EXT_REF0_PINS, ADS1220_LOW_SIDE_POWER_OPEN, ADS1220_IDAC_CURRENT_250_UA, 
                                        ADS1220_IDAC1_AIN3, ADS1220_IDAC2_AIN2, ADS1220_DRDY_ON_DRDY_ONLY);
    //readback or delay
    delay(1);
    myADS1220_647.ADS1220_Start();
    //Delay for a minimum of td(SCCS);
    delay(1);
    //clear /CS to high
    digitalWrite(CS, HIGH);
    
    pinMode(LED, OUTPUT);   
    digitalWrite(LED, HIGH);
    Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps


}

long Resistor(long adc_code)
{
  float Vin, Rin;
  float Vrefp = 3.24e3*250e-6*2;
  Vin = adc_code *Vrefp/(8388607)/16;
  Rin = Vin/250e-6;
  return Rin;
}

long Temperature(long adc_code)
{
  float Vin, Tin;

  Vin = adc_code >> 10;
  Tin = Vin*0.03125;
  return Tin;
}

void loop() {
    float * data;
    long code;
    float res;
   
    delay(100);
    digitalWrite(LED, HIGH);
    delay(100);
    digitalWrite(LED, LOW);
  
    if (count>0 )
    {

    myADS1220_647.ADS1220_Get_Conversion_Data ((unsigned char *)tempData);

    code = (((long)tempData[0] << 16) + ((long)tempData[1] << 8) + (long)tempData[2]);
    Serial.println(code);

      if (temp==0 && count>0) {
        Serial.print("Resistor");
        res = Resistor(code);
        Serial.println(res);
        count = 0;
        temp=1;
        myADS1220_647.ADS1220_next_read_internal_temp();
      }
      if (temp==1 && count> 0) {
        Serial.print("Temp");
        res = myADS1220_647.InternalTemp(code);
        Serial.println(res);
        count = 0;
        temp=0;
        myADS1220_647.ADS1220_restore_after_internal_temp_read();
      }
    }
}

void counting()
{
  count+=1;
}
