/* --COPYRIGHT--,BSD
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/*******************************************************************************
 *
 * ADS1220.cpp
 *
 * Hardware abstraction layer for the ADS1220
 *
 * Aug 2015
 * MCV (Matt-CHV)
 *
 *
 ******************************************************************************/

#include <SPI.h>
#include <ADS1220.h>



ADS1220::ADS1220(int ads1220_cs_pin)
{
	//pinMode(pin, OUTPUT);
	_ads1220_cs = ads1220_cs_pin;
}

void ADS1220::init (unsigned char inputMux, unsigned char opMode,
                    unsigned char conversionMode, unsigned char dataRate, unsigned char gainLevel, unsigned char pgaBypass,
                    unsigned char routeIDAC1, unsigned char routeIDAC2, unsigned char idacCurrent)

{

    unsigned char config[4];

    config[0] = inputMux + gainLevel + pgaBypass;
    config[1] = dataRate + opMode + conversionMode + ADS1220_TEMP_SENSOR_OFF + ADS1220_BURN_OUT_CURRENT_OFF;
    config[2] = ADS1220_FIR_50_60 + ADS1220_VREF_EXT_REF0_PINS + ADS1220_LOW_SIDE_POWER_OPEN + idacCurrent;
    config[3] = routeIDAC1 + routeIDAC2 + ADS1220_DRDY_ON_DRDY_ONLY;

    ADS1220_Write_Regs (config, ADS1220_CONFIG_0_REG, 4);
}

void ADS1220::ADS1220_initfull(unsigned char inputMux, unsigned char gainLevel, unsigned char pgaBypass,
					unsigned char dataRate, unsigned char opMode, unsigned char conversionMode,  unsigned char temp_sensor_onoff, unsigned char burnout_current,
					unsigned char fir_50_60, unsigned char vref, unsigned char low_side_switch, unsigned char idacCurrent, 
                    unsigned char routeIDAC1, unsigned char routeIDAC2, unsigned char drdy )
{
	//FIXME: here we are still hard coding the FIR_50_60, ...

	unsigned char config[4];

    config[0] = inputMux + gainLevel + pgaBypass;
    config[1] = dataRate + opMode + conversionMode + temp_sensor_onoff + burnout_current;
    config[2] = fir_50_60 + vref + low_side_switch + idacCurrent;
    config[3] = routeIDAC1 + routeIDAC2 + drdy;

    ADS1220_Write_Regs (config, ADS1220_CONFIG_0_REG, 4);
	
}

void ADS1220::ADS1220_next_read_internal_temp()
{
	unsigned char config[4];
	unsigned char conf_reg_1;
	ADS1220_Read_Regs(config, ADS1220_CONFIG_1_REG,1);
	conf_reg1_backup = config[1];
	config[0] =  config[1]+ADS1220_TEMP_SENSOR_ON ;
	//config[0] = ADS1220_DATA_RATE_20SPS + ADS1220_OP_MODE_NORMAL + ADS1220_CONVERSION_CONTINUOUS + ADS1220_TEMP_SENSOR_ON + ADS1220_BURN_OUT_CURRENT_OFF;
	ADS1220_Write_Regs (config, ADS1220_CONFIG_1_REG, 1);
}

void ADS1220::ADS1220_restore_after_internal_temp_read()
{
	unsigned char config[4];
	ADS1220_Read_Regs(config, ADS1220_CONFIG_1_REG,1);
	config[0] = config[1] & ~(ADS1220_TEMP_SENSOR_ON);
//	config[0] = ADS1220_DATA_RATE_20SPS + ADS1220_OP_MODE_NORMAL + ADS1220_CONVERSION_CONTINUOUS + ADS1220_TEMP_SENSOR_OFF + ADS1220_BURN_OUT_CURRENT_OFF;
	ADS1220_Write_Regs (config, ADS1220_CONFIG_1_REG, 1);
}

long ADS1220::InternalTemp(long ADCCode_24b)
{
	return (ADCCode_24b>>10)*0.03125;
}

void ADS1220::ADS1220_Reset (void)
{
	static unsigned char Rcv_Data[8];
    unsigned char cmd = ADS1220_RESET_CMD;
	
	_SPI_write (&cmd, Rcv_Data, 1);
}


void ADS1220::ADS1220_Start (void)
{
	static unsigned char Rcv_Data[8];

    unsigned char cmd = ADS1220_START_CMD;
    _SPI_write (&cmd, Rcv_Data, 1);
}

void ADS1220::ADS1220_Powerdown (void)
{
	static unsigned char Rcv_Data[8];
	
    unsigned char cmd = ADS1220_POWERDOWN_CMD;
    _SPI_write (&cmd, Rcv_Data, 1);
}


void ADS1220::ADS1220_Send_Read_Data_Command (void)
{
	unsigned char Rcv_Data[8];
	
    unsigned char cmd = ADS1220_RDATA_CMD;
    _SPI_write (&cmd, Rcv_Data, 1);
}

void ADS1220::ADS1220_Get_Conversion_Data (unsigned char *conversionData)
{

    //unsigned char outData[3] = {0xff, 0xff, 0xff};
	unsigned char outData[3] = {0xaa, 0xaa, 0xaa};

    _SPI_write (outData, conversionData, 3);    // 3 Bytes of Conversion Data

}

void ADS1220::ADS1220_Read_Regs (unsigned char *readValues, unsigned char startReg, unsigned char length)
{
    unsigned char outData[5] = {0x55, 0x55, 0x55, 0x55, 0x55};

    outData[0] = ADS1220_READ_CMD(startReg,length);

    _SPI_write (outData, readValues, length+1);    // Add 1 to length for command byte

}

void ADS1220::ADS1220_Write_Regs (unsigned char *writeValues, unsigned char startReg, unsigned char length)
{
    unsigned char outData[5];
    unsigned char i;
	unsigned char Rcv_Data[8];

    outData[0] = ADS1220_WRITE_CMD(startReg,length);

    for (i=0; i<length; i++)
    {
        outData[i+1] = writeValues[i];
    }

    _SPI_write (outData, Rcv_Data, length+1);    // Add 1 to length for command byte

}

void ADS1220::_SPI_write (unsigned char *outData, unsigned char *inData, unsigned char length)
{
unsigned char i;
	delay(1);
    digitalWrite(_ads1220_cs, LOW);
    delay(1);
    for (i=0; i<length; i++)
    {
        inData[i] = SPI.transfer(outData[i]);
    }
	delay(1);
    digitalWrite(_ads1220_cs, LOW);
	
}


