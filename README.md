# TIDA-00647 Energia Library

## Energia Install
follow Energia [online](http://energia.nu/download/) instructions

## Import the ADS1220 library in Energia
TL;DR : In Energia import the folder where the README is located as the Sketchbook location.

Alternatively refer to either: 
* [TIDA-00647](http://www.ti.com/tool/TIDA-00647) User's Guide 
* Energia [libraries](https://energia.nu/guide/libraries/) online page